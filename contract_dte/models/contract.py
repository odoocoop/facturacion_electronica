# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)


class Contract(models.Model):
    _inherit = 'contract.contract'


    def _default_use_documents(self):
        if self._default_journal_document_class_id():
            return True
        return False

    @api.onchange('journal_id')
    @api.depends('journal_id')
    def _get_dc_ids(self):
        for r in self:
            r.document_class_ids = [j.sii_document_class_id.id for j in r.journal_id.journal_document_class_ids.filtered(lambda x: x.sii_document_class_id.document_type == 'invoice')]

    document_class_ids = fields.Many2many(
        "sii.document_class", compute="_get_dc_ids", string="Available Document Classes",
    )
    journal_document_class_id = fields.Many2one(
        "account.journal.sii_document_class",
        string="Documents Type",
        domain="[('sii_document_class_id', '=', document_class_ids)]",
    )
    use_documents = fields.Boolean(
        string="Use Documents?",
    )

    @api.onchange('journal_id')
    def set_use_documents(self):
        self.journal_document_class_id = self.env["account.journal.sii_document_class"].search(
            [("journal_id", "=", self.journal_id.id), ("sii_document_class_id.document_type", "in", ['invoice']),], limit=1
        )
        self.use_documents = bool(self.journal_document_class_id)

    def _prepare_invoice(self, date_invoice, journal=None):
        vals = super(Contract, self)._prepare_invoice(date_invoice, journal)
        if self.recurring_invoicing_type == 'pre-paid':
            narration = f"Periodo {self.last_date_invoiced} al {vals['invoice_date']}"
        else:
            narration = f"Periodo {vals['invoice_date']} al {self.recurring_next_date}"
        if self.note:
            narration = f"{narration} \n {self.note}"
        vals.update({
            'journal_document_class_id': self.journal_document_class_id.id,
            'use_documents': self.use_documents,
            'document_class_id': self.journal_document_class_id.sii_document_class_id.id,
            'narration': narration,
            'name': '/',
        })
        return vals

    ''' Esta sección necesita del Módulo contract_sale_generation instalado'''
    def _prepare_sale(self, date_ref):
        vals = super(Contract, self)._prepare_sale()
        date_sale = fields.Date.to_string(date_ref)
        if self.recurring_invoicing_type == 'pre-paid':
            narration = f"Periodo {self.last_date_invoiced} al {vals['date_order']}"
        else:
            narration = f"Periodo {vals['date_order']} al {self.recurring_next_date}"
        if self.note:
            narration = f"{narration} \n {self.note}"
        vals.update({
            'journal_id': self.journal_id.id,
            'journal_document_class_id': self.journal_document_class_id.id,
            'use_documents': self.use_documents,
            'document_class_id': self.journal_document_class_id.sii_document_class_id.id,
            'note': narration,
        })
        return vals
