## Contract DTE

Contract DTE es un módulo para Odoo que permite agregar los datos necesarios para la localización chilena (l10n_cl_fe) en los contratos del módulo de la OCA contract.

## Descripción

Contract DTE extiende las funcionalidades del módulo contract de la OCA, integrando los requerimientos específicos para la facturación electrónica chilena (l10n_cl_fe). Este módulo facilita la gestión de contratos cumpliendo con la normativa local, permitiendo la emisión de documentos tributarios electrónicos (DTE) con los parámetros DTE configurados en el contrato.


## Características

- Integración con l10n_cl_fe: Añade los campos y funcionalidades necesarios para cumplir con la facturación electrónica chilena en los contratos.
- Compatibilidad con OCA contract: Funciona de manera fluida con el módulo contract de la OCA.
- Automatización de DTE: Genera y envía DTE automáticamente al crear o actualizar contratos.
- Validación de datos: Asegura que todos los datos requeridos para la localización chilena estén completos y sean válidos.
- Interfaz amigable: Añade campos adicionales en la vista de contratos para una gestión más eficiente.

## Capturas de pantalla

###    Clonar el repositorio:

```bash

git clone https://gitlab.com/dansanti/contract_dte.git

```

Mover el módulo a la carpeta de addons de Odoo:

```bash

    mv contract_dte /ruta/a/tu/odoo/addons/

```
    Actualizar la lista de módulos:

    Inicia sesión en Odoo, ve a Aplicaciones y haz clic en Actualizar lista de aplicaciones.

    Instalar el módulo:

    Busca Contract DTE y haz clic en Instalar.

## Configuración

    Configurar l10n_cl_fe:

    Asegúrate de tener instalado y configurado el módulo l10n_cl_fe para la facturación electrónica chilena.

    Configurar el módulo contract:

    Instala y configura el módulo contract de la OCA si aún no lo has hecho.



## Uso

### Crear un contrato:

- Ve a Contratos y crea un nuevo contrato como de costumbre.

- Completar los datos de DTE:

- Rellena los campos adicionales proporcionados por el módulo Contract DTE, como tipo de DTE, fecha de emisión, cliente, etc.

### Generar DTE:

 - Una vez creado o actualizado el contrato, el módulo generará automáticamente el DTE correspondiente y lo enviará al Servicio de Impuestos Internos (SII).

 - Revisar el estado del DTE:

 - Puedes verificar el estado de los DTE generados desde la vista del contrato, donde se mostrará las facturas generadas  y si rechazados o están en proceso.

## Dependencias

    Odoo: Compatible con las versiones 16.0.
    Módulo [l10n_cl_fe]: https://gitlab.com/dansanti/l10n_cl_fe Localización chilena para facturación electrónica.
    Módulo contract de la OCA: [OCA/contract] https://github.com/OCA/contract.

## Contribución

¡Contribuciones son bienvenidas! Si deseas contribuir, por favor sigue estos pasos:

    Fork este repositorio.
    Crea una rama para tu característica o corrección de bug (git checkout -b feature/nueva-característica).
    Commit tus cambios (git commit -m 'Añadir nueva característica').
    Push a la rama (git push origin feature/nueva-característica).
    Abre un Pull Request describiendo tus cambios.

Por favor, asegúrate de que tus contribuciones sigan las guías de estilo del código y que pasen todas las pruebas existentes.

## Licencia

Este proyecto está licenciado bajo la licencia AGPL-3. Consulta el archivo LICENSE para más detalles.

Desarrollado por [GlobalResponse] https://globalresponse.cl. Para más información.
