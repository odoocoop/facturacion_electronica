# -*- coding: utf-8 -*-
{
    'name':'Contract DTE',
    'summary': 'Default DTE options in contracts',
    'description':'Default Chilean DTE options for OCA module contract',
    'category': 'Contract Management',
    'countries': ['cl'],
    'version':'0.0.1',
    'website':'https://globalresponse.cl',
    "license": "AGPL-3",
    'author':'Daniel Santibáñez Polanco',
    'data': [
        'views/contract_contract.xml',
    ],
    'depends': [
        'contract',
        'l10n_cl_fe'
    ],
    'qweb': [],
    'application': True,
}
